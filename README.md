# Astraeus Media Server #

This software was designed to operate live entertainment digital backgrounds.

Release versions of this software have run flawlessly on several shows.

The name Astraeus was chosen for the software for no particular reason other than it sounded cool. It is a nod to the Greek God Astraeus. Although it should be of interest that in it's debut show, it did show an image of the Milky Way Galaxy.

## How it works

This software has a very limited front-end.

At present, it's a hybrid of application screen for configuration and a web-based media manager.  The application screens control what network interface is used to listen to either sACN or ArtNet. It also then is used to specify which universe to listen to and how many layers and which addresses are used to control those layers.

A typical setup will use two or more layers. Layer one is the top layer - the one closest to the audience. Layer two is right behind that layer, and so forth.

## Server Installation ##

Astraeus runs on Java 8 and uses JavaFX. It runs very well on Apple Macintosh computers. It's also run well on Windows 7 computers, but I've experenced issues with it on Windows 10.

Install the most recent version of Java 8.

### 

